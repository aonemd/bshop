<?
if(isset($_POST['SignInSubmit'])) {
  session_start();

  include("../db/database.php");

  $email = $_POST['email'];
  $password = $_POST['password'];

  $query = "SELECT id, is_admin FROM users WHERE email='$email' AND password='$password' LIMIT 1";
  $result = mysql_query($query, $connection);

  if(mysql_num_rows($result) == 1) {
    $user = mysql_fetch_array($result);

    $_SESSION['userID'] = $user['id'];

    if($user['is_admin'] == 1) {
      $_SESSION['userIsAdmin'] = $user['is_admin'];
    }

    header('location: ../products/index.php');
  } else {
    $_SESSION['notice'] = "Invalid Credentials";
  }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Sign in</title>
  <link href="../style/style.css" rel="stylesheet"> 
  <link href="../style/foundation-5.5.2/css/foundation.css" rel="stylesheet"> 
  <script src="../style/foundation-5.5.2/js/vendor/modernizr.js"></script>
</head>
<body>

<!-- topbar -->
<? include("../includes/header.php"); ?>
<!-- end of topbar -->

<div class="large-3 large-centered columns">
  <div class="form-box">
    <div class="row">
      <div class="large-12 columns">
        <form action="" method="post">
          <div class="row">
            <div class="large-12 columns">
              <div class="form-element">
                <h3 class="text-center">Sign In</h3>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="large-12 columns">
              <div class="form-element">
                <input type="email" name="email" placeholder="Email">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="large-12 columns">
              <div class="form-element">
                <input type="password" name="password" placeholder="Password">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="large-12 large-centered columns">
              <div class="form-element">
                <input type="submit" name="SignInSubmit" value="Sign In" class="button radius expand">
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <div class="text-center">
    Don't have an account?
    <a href="sign_up.php"> Sign Up</a>
  </div>
</div>

<script src="../style/foundation-5.5.2/js/vendor/jquery.js"></script>
<script src="../style/foundation-5.5.2/js/vendor/fastclick.js"></script>
<script src="../style/foundation-5.5.2/js/foundation.min.js"></script>
<script src="../style/foundation-5.5.2/js/foundation.min.js"></script>
<script> $(document).foundation(); </script>
</body>
</html>
