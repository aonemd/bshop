<?
session_start();
if(!isset($_SESSION["userID"])) {
  $_SESSION['notice'] = "You are Not authorized to view this page";
  header("location: sign_in.php");
  exit();
}

include("../db/database.php");
include("../helpers/users_helper.php");
include("../helpers/image_uploader.php");

$id = $_SESSION["userID"];
$user = getUserByID($id);

if(isset($_POST['EditSubmit'])) {
  $email = $_POST['email'] ? mysql_real_escape_string($_POST['email']) : $user['email'];
  $password = $_POST['password'] ? mysql_real_escape_string($_POST['password']) : $user['password'];
  $firstName = $_POST['firstName'] ? mysql_real_escape_string($_POST['firstName']) : $user['firstName'];
  $lastName = $_POST['lastName'] ? mysql_real_escape_string($_POST['lastName']) : $user['lastName'];
  
  // check if a new image file is present
  if (!empty($_FILES["avatarURL"]["name"])) {
    $avatarURL = upload_image("avatarURL", "../public/users/");
  } else {
    $avatarURL = $user['avatarURL'];
  }

// update user profile
$query = "UPDATE users SET email='$email', password='$password', 
  first_name='$firstName', last_name='$lastName', 
  avatar_url='$avatarURL' WHERE id='$id'";

  if(!mysql_query($query, $connection)){
    echo "error".mysql_error()."\n";
  }

  $_SESSION['notice'] = "Changes Saved";
  header("location: profile.php");
  EXIT;
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Edit Profile</title>
  <link href="../style/style.css" rel="stylesheet"> 
  <link href="../style/foundation-5.5.2/css/foundation.css" rel="stylesheet"> 
  <script src="../style/foundation-5.5.2/js/vendor/modernizr.js"></script>
</head>
<body>

<!-- topbar -->
<? include("../includes/header.php"); ?>
<!-- end of topbar -->

<div class="large-3 large-centered columns">
  <div class="form-box">
    <div class="row">
      <div class="large-12 columns">
        <form action="" method="post" enctype="multipart/form-data" class="text-center">
          <div class="row">
            <div class="large-12 columns">
              <div class="form-element">
                <div class="circular-avatar">
                  <img src="<?= $user['avatarURL'] ?>" alt="User Avatar">
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="large-12 columns">
              <div class="form-element">
              <input type="email" name="email" placeholder="<?= $user['email'] ?>">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="large-12 columns">
              <div class="form-element">
                <input type="password" name="password" placeholder="Password (Unchanged)">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="large-12 columns">
              <div class="form-element">
              <input type="text" name="firstName" placeholder="<?= $user['firstName'] ?>">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="large-12 columns">
              <div class="form-element">
              <input type="text" name="lastName" placeholder="<?= $user['lastName'] ?>">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="large-12 columns">
              <div class="form-element">
                Change Avatar <input type="file" name="avatarURL" placeholder="Profile Picture">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="large-12 large-centered columns">
              <div class="form-element">
                <input type="submit" name="EditSubmit" value="Save Profile" class="button radius expand">
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<script src="../style/foundation-5.5.2/js/vendor/jquery.js"></script>
<script src="../style/foundation-5.5.2/js/vendor/fastclick.js"></script>
<script src="../style/foundation-5.5.2/js/foundation.min.js"></script>
<script src="../style/foundation-5.5.2/js/foundation.min.js"></script>
<script> $(document).foundation(); </script>
</body>
</body>
</html>
