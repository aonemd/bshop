<?
session_start();
if(!isset($_SESSION["userID"])) {
  $_SESSION['notice'] = "You are Not authorized to view this page";
  header("location: sign_in.php");
  exit();
}
include("../helpers/users_helper.php");

$user = getUserByID($_SESSION["userID"]);

$membershipStatus = getUserMembershipStatus($user['isAdmin']);
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title><?= $user['firstName']." ".$user['lastName']; ?> Profile</title>
  <link href="../style/style.css" rel="stylesheet"> 
  <link href="../style/foundation-5.5.2/css/foundation.css" rel="stylesheet"> 
  <script src="../style/foundation-5.5.2/js/vendor/modernizr.js"></script>
</head>
<body>
<!-- topbar -->
<? include("../includes/header.php"); ?>
<!-- end of topbar -->

<div class="large-3 large-centered columns">
  <div class="form-box">
    <div class="row">
      <div class="large-12 columns">
        <form class="text-center">
          <div class="row">
            <div class="large-12 columns">
              <div class="form-element">
                <div class="circular-avatar">
                  <img src="<?= $user['avatarURL'] ?>" alt="User Avatar">
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="large-12 columns">
              <div class="form-element">
              <h3 class="<?= $membershipStatus['class'] ?>"><?= $membershipStatus['status'] ?></h3>
              </div>
            </div>
          <div class="row">
            <div class="large-12 columns">
              <div class="form-element">
                Email<h5><?= $user['email'] ?></h5>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="large-12 columns">
              <div class="form-element">
                Full Name<h5><?= $user['firstName']." ".$user['lastName'] ?></h5>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="large-12 large-centered columns">
              <div class="form-element">
                User Since<h5><?= date("F d, Y", strtotime($user['createdAt'])) ?></h5>
              </div>
            </div>
          </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <? if($user['isAdmin']) { ?>
    <div class="text-center">
      <a href="/products/new.php"> Add New Product</a><br>
    </div>
    <br>
  <? } ?>
</div>

<script src="../style/foundation-5.5.2/js/vendor/jquery.js"></script>
<script src="../style/foundation-5.5.2/js/vendor/fastclick.js"></script>
<script src="../style/foundation-5.5.2/js/foundation.min.js"></script>
<script src="../style/foundation-5.5.2/js/foundation.min.js"></script>
<script> $(document).foundation(); </script>
</html>
