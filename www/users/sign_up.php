<?
if(isset($_POST['SignUpSubmit'])) {
  include("../db/database.php");
  include("../helpers/image_uploader.php");

  $email = mysql_real_escape_string($_POST['email']);
  $password = mysql_real_escape_string($_POST['password']);
  $first_name = mysql_real_escape_string($_POST['firstName']);
  $last_name = mysql_real_escape_string($_POST['lastName']);

  $query = "INSERT INTO users (email, password, first_name, last_name, created_at)
            VALUES ('$email', '$password', '$first_name', '$last_name', now())";

  if(mysql_query($query, $connection)) {
    // upload avatar image
    $id = mysql_insert_id();

    if ($imagePath = upload_image("avatarURL", "../public/users/")) {
      $query = "UPDATE users SET avatar_url='$imagePath' WHERE id='$id'";
      if(!mysql_query($query, $connection)){
        echo "error".mysql_error()."\n";
      }
    }

    $_SESSION['notice'] = "Signed Up successfully.";
    header("location: sign_in.php");
  } else {
    $_SESSION['notice'] = "User error: ".mysql_error()."\n";
  }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Sign Up</title>
  <link href="../style/style.css" rel="stylesheet"> 
  <link href="../style/foundation-5.5.2/css/foundation.css" rel="stylesheet"> 
  <script src="../style/foundation-5.5.2/js/vendor/modernizr.js"></script>
</head>
<body>

<!-- topbar -->
<? include("../includes/header.php"); ?>
<!-- end of topbar -->

<div class="large-3 large-centered columns">
  <div class="form-box">
    <div class="row">
      <div class="large-12 columns">
        <form action="" method="post" enctype="multipart/form-data">
          <div class="row">
            <div class="large-12 columns">
              <div class="form-element">
                <h3 class="text-center">Sign Up</h3>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="large-12 columns">
              <div class="form-element">
                <input type="email" name="email" placeholder="Email">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="large-12 columns">
              <div class="form-element">
                <input type="password" name="password" placeholder="Password">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="large-12 columns">
              <div class="form-element">
                <input type="text" name="firstName" placeholder="First Name">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="large-12 columns">
              <div class="form-element">
                <input type="text" name="lastName" placeholder="Last Name">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="large-12 columns">
              <div class="form-element">
                <input type="file" name="avatarURL" placeholder="Profile Picture">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="large-12 large-centered columns">
              <div class="form-element">
                <input type="submit" name="SignUpSubmit" value="Sign Up" class="button radius expand">
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <div class="text-center">
    Already have an account?
    <a href="sign_in.php"> Sign In</a>
  </div>
</div>

<script src="../style/foundation-5.5.2/js/vendor/jquery.js"></script>
<script src="../style/foundation-5.5.2/js/vendor/fastclick.js"></script>
<script src="../style/foundation-5.5.2/js/foundation.min.js"></script>
<script src="../style/foundation-5.5.2/js/foundation.min.js"></script>
<script> $(document).foundation(); </script>
</body>
</html>
