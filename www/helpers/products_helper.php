<?
function getProductByID($id) {
  include("../db/database.php");

  $query = "SELECT * FROM products WHERE id='$id' LIMIT 1";
  $result = mysql_query($query, $connection);

  $row = mysql_fetch_array($result);

  $product = ['id' => $row['id'], 'name' => $row['name'], 'price' => $row['price'], 
    'quantity' => $row['quantity'], 'description' => $row['description'], 
    'imageURL' => $row['image_url'], 'createdAt' => $row['created_at']];

  return $product;
}

function isAvailable($product) {
  if($product['quantity'] > 0) {
    return true;
  } else {
    return false;
  }
}

function toggleButton($product, $msg) {
  if(isAvailable($product)) {
    $buttonStatus = [status => '', msg => $msg];
  } else {
    $buttonStatus = [status => 'disabled', msg => 'Sold Out'];
  }

  return $buttonStatus;
}
?>
