<?
if (isset($_SESSION['cart']))
  $cart = $_SESSION['cart'];

function add_item($product) {
  global $cart;

  if($product['quantity'] == 1)
    lock($product);

  if(!isset($cart) || count($cart) < 1) {
    // cart is empty, were're adding the first item
    $product['quantity'] = 1;
    $cart = array($product);
  } else {
      // cart has some items
      foreach($cart as &$item) {
        if($item['id'] == $product['id']) {
          // item is already in the cart
          if(($product['quantity'] - $item['quantity']) > 1) {
            // add the item only if there's enough of it
            $item['quantity'] += 1;

            $_SESSION['cart'] = $cart;
          } else if(($product['quantity'] - $item['quantity']) == 1) {
            $item['quantity'] += 1;

            $_SESSION['cart'] = $cart;

            lock($product);
          } else {
            lock($product);
          }
          return;
        }
    }

    // item is not in the current cart
    $product['quantity'] = 1;
    array_push($cart, $product);
  }

  $_SESSION['cart'] = $cart;
}

function checkout() {
  global $cart;

  include("../db/database.php");

  // update product quantities in the database
  foreach($cart as $item) {
    $pid = $item['id'];
    $product = getProductByID($pid);

    $quantity = $product['quantity'] - $item['quantity'];

    $query = "UPDATE products SET quantity='$quantity' WHERE id='$pid'";

    if(!mysql_query($query, $connection)){
      echo "error".mysql_error()."\n";
    }
  }

  // create order
  $userID = $_SESSION['userID'];
  $totalAmount = total_amount();
  $cart = serialize($cart);

  $query = "INSERT INTO orders (cart, user_id, total_amount, created_at)
            VALUES ('$cart', '$userID', '$totalAmount', now())";

  if(!mysql_query($query, $connection)){
    echo "error".mysql_error()."\n";
  }

  empty_cart();
}

function remove_item($product) {
  global $cart;

  foreach($cart as $key => &$item) {
    if($item['id'] == $product['id']) {
      if($item['quantity'] > 1) {
        $item['quantity'] -= 1;
      } else {
        unset($cart[$key]);
      }
      // found the item, get out
      break;
    }
  }

  // normalize cart keys
  $cart = array_values($cart);
  $_SESSION['cart'] = $cart;

  // unlock product button
  unlock($product);
}

function empty_cart() {
  unset($_SESSION['cart']);
  unset($_SESSION['lockedButtons']);
}

function count_items() {
  global $cart;

  $counter = 0;

  foreach($cart as $item) {
    $counter += $item['quantity'];
  }

  return $counter;
}

function total_amount() {
  global $cart;

  $total = 0;

  foreach($cart as $item) {
    $total += $item['quantity'] * $item['price'];
  }

  return $total;
}

function lock($product) {
  $lockedButtons = $_SESSION['lockedButtons']; 

  if(!isset($lockedButtons) || count($lockedButtons) < 1) {
    $lockedButtons = array($product['id']);
  } else {
    if(in_array($product['id'], $lockedButtons) === false ) {
      array_push($lockedButtons, $product['id']);
    }
  }
  $_SESSION['lockedButtons'] = $lockedButtons;
}

function unlock($product) {
  $lockedButtons = $_SESSION['lockedButtons']; 

  $key = array_search($product['id'], $lockedButtons);
  if($key !== false){
    unset($lockedButtons[$key]);
  }

  // normalize lockedButton keys
  $lockedButtons = array_values($lockedButtons);
  $_SESSION['lockedButtons'] = $lockedButtons;
}
?>
