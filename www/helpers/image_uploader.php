<?
function getImageExtension($imageType) {
  if(empty($imageType)) return false;

  switch($imageType) {
    case 'image/bmp': return '.bmp';
    case 'image/gif': return '.gif';
    case 'image/jpeg': return '.jpg';
    case 'image/png': return '.png';
    default: return false;
  }
}

function upload_image($fieldName, $targetDir) {
  if (!empty($_FILES["$fieldName"]["name"])) {
    $fileName = $_FILES["$fieldName"]["name"];
    $tmpName = $_FILES["$fieldName"]["tmp_name"];
    $imgType = $_FILES["$fieldName"]["type"];
    $ext = getImageExtension($imgType);
    $imageName = date("d-m-Y")."-".time().$ext;
    $targetPath = $targetDir.$imageName;

    move_uploaded_file($tmpName, $targetPath);

    return $targetPath;
  }
}
?>
