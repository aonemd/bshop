<?
function getUserByID($id) {
  include("../db/database.php");

  $query = "SELECT * FROM users WHERE id='$id' LIMIT 1";
  $result = mysql_query($query, $connection);

  $row = mysql_fetch_array($result);

  $user = ['email' => $row['email'], 'firstName' => $row['first_name'], 
    'lastName' => $row['last_name'], 'createdAt' => $row['created_at'], 
    'isAdmin' => $row['is_admin'], 'avatarURL' => $row['avatar_url']];

  return $user;
}

function getUserMembershipStatus($isAdmin) {
  if($isAdmin == 1) {
    $membershipStatus['status'] = 'Admin';
    $membershipStatus['class'] = 'admin-user';
  } else {
    $membershipStatus['status'] = 'User';
    $membershipStatus['class'] = 'regular-user';
  }

  return $membershipStatus;
}

?>
