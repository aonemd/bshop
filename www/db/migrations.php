<?php
include("database.php");

// products table
$create_products_table_query = "CREATE TABLE products (
                        id INT(11) NOT NULL AUTO_INCREMENT,
                        name VARCHAR(50) NOT NULL,
                        price DECIMAL(7,2) NOT NULL DEFAULT 99999.99,
                        quantity INT UNSIGNED NOT NULL DEFAULT 0,
                        description TEXT,
                        image_url VARCHAR(255),
                        created_at DATETIME NOT NULL,
                        PRIMARY KEY (id),
                        UNIQUE KEY name (name)
                        )";

if (mysql_query($create_products_table_query, $connection)) {
  echo "Products table was created.\n";
} else {
    echo "Products table error: ".mysql_error()."\n";
}

// users table
$create_users_table_query = "CREATE TABLE users (
                        id INT(11) NOT NULL AUTO_INCREMENT,
                        email VARCHAR(24) NOT NULL,
                        first_name VARCHAR(100) NOT NULL,
                        last_name VARCHAR(100) NOT NULL,
                        password VARCHAR(24) NOT NULL,
                        avatar_url VARCHAR(255),
                        created_at DATETIME NOT NULL,
                        is_admin TINYINT(1) DEFAULT 0,
                        PRIMARY KEY (id),
                        UNIQUE KEY email (email)
                        )";


if (mysql_query($create_users_table_query)) {
  echo "Users table was created.\n";
} else {
    echo "Users table error: ".mysql_error()."\n";
}

// orders table
$create_orders_table_query = "CREATE TABLE orders (
                        id INT(11) NOT NULL AUTO_INCREMENT,
                        cart BLOB NOT NULL,
                        user_id INT(11) NOT NULL,
                        total_amount DECIMAL(7,2) NOT NULL DEFAULT 99999.99,
                        created_at DATETIME NOT NULL,
                        PRIMARY KEY (id)
                        )";


if (mysql_query($create_orders_table_query)) {
  echo "Orders table was created.\n";
} else {
    echo "Orders table error: ".mysql_error()."\n";
}
?>
