<?php
require "database.php";

// users
$query = "INSERT INTO users (email, first_name, last_name, password, avatar_url, created_at, is_admin)
          VALUES ('admin@bshop.com', 'Ahmed', 'S. Abdelwahab', '12345678', '../public/users/02-10-2015-1443750999.jpg', now(), '1')";

if(mysql_query($query, $connection)) {
  echo "Admin User was created.\n";
} else {
  echo "Admin User error: ".mysql_error()."\n";
}

$query = "INSERT INTO users (email, first_name, last_name, password, avatar_url, created_at)
          VALUES ('leila.asaleh@gmail.com', 'Leila', 'A. Saleh', '12345678', '../public/users/02-10-2015-1443745586.png', now())";

if(mysql_query($query, $connection)) {
  echo "Regular User was created.\n";
} else {
  echo "Regular error: ".mysql_error()."\n";
}

$query = "INSERT INTO users (email, first_name, last_name, password, avatar_url, created_at)
          VALUES ('saeed@air.com', 'Saeed', 'The Air', '12345678', '../public/users/01-10-2015-1443726444.jpg', now())";

if(mysql_query($query, $connection)) {
  echo "Regular User was created.\n";
} else {
  echo "Regular error: ".mysql_error()."\n";
}

// products
$query = "INSERT INTO products (name, price, quantity, description, image_url, created_at)
          VALUES ('Dina Farms Milk', '8', '2', 'Dina Farms Fresh Milk', '../public/products/01-10-2015-1443731397.jpg', now())";

if (mysql_query($query, $connection)) {
  echo "Product was created.\n";
} else {
    echo "Product error: ".mysql_error()."\n";
}

$query = "INSERT INTO products (name, price, quantity, description, image_url, created_at)
          VALUES ('Teddy Bear', '22', '1', 'Cotton Teddy Bear', '../public/products/01-10-2015-1443733723.jpg', now())";

if (mysql_query($query, $connection)) {
  echo "Product was created.\n";
} else {
    echo "Product error: ".mysql_error()."\n";
}

$query = "INSERT INTO products (name, price, quantity, description, image_url, created_at)
          VALUES ('Fluffy Unicorn', '89', '3', 'Fluffy and soft Unicorn', '../public/products/01-10-2015-1443733747.jpg', now())";

if (mysql_query($query, $connection)) {
  echo "Product was created.\n";
} else {
    echo "Product error: ".mysql_error()."\n";
}

$query = "INSERT INTO products (name, price, quantity, description, image_url, created_at)
          VALUES ('Ninja Tabi Shoes', '35', '8', 'Comfort-Cushioned Ninja Tabi Shoes! (Japanese Low Tops - Black Rikio JikaTabi', '../public/products/06-10-2015-1444143589.jpg', now())";

if (mysql_query($query, $connection)) {
  echo "Product was created.\n";
} else {
    echo "Product error: ".mysql_error()."\n";
}

$query = "INSERT INTO products (name, price, quantity, description, image_url, created_at)
          VALUES ('How I Became Stupid', 
          '6.96', '1', 
          'Ignorance is bliss, or so hopes Antoine, the lead character in Martin Page?s stinging satire, How I Became Stupid?a modern day Candide with a Darwin Award?like sensibility. A twenty-five-year-old Aramaic scholar, Antoine has had it with being brilliant and deeply self-aware in today?s culture. So tortured is he by the depth of his perception and understanding of himself and the world around him that he vows to denounce his intelligence by any means necessary?in order to become ?stupid? enough to be a happy, functioning member of society. What follows is a dark and hilarious odyssey as Antoine tries everything from alcoholism to stock-trading in order to lighten the burden of his brain on his soul.', '../public/products/06-10-2015-1444144944.jpg', 
          now())";

if (mysql_query($query, $connection)) {
  echo "Product was created.\n";
} else {
    echo "Product error: ".mysql_error()."\n";
}

$query = "INSERT INTO products (name, price, quantity, description, image_url, created_at)
          VALUES ('Taylor 314ce', '1799', '6', 'Taylor Guitars 314ce Grand Auditorium Acoustic Electric Guitar', '../public/products/06-10-2015-1444145120.jpg', now())";

if (mysql_query($query, $connection)) {
  echo "Product was created.\n";
} else {
    echo "Product error: ".mysql_error()."\n";
}
?>
