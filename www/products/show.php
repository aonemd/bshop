<?
include("../helpers/products_helper.php");

$id = $_GET['id'];

$product = getProductByID($id);

session_start();
if(in_array($id, $_SESSION['lockedButtons'])) {
  $buttonStatus = 'disabled';
  $buttonMsg = 'Sold Out';
} else {
  $buttonStatus = toggleButton($product, 'Add To Cart')['status'];
  $buttonMsg = toggleButton($product, 'Add To Cart')['msg'];
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title><?= $product['name'] ?> | BShop</title>
  <link href="../style/style.css" rel="stylesheet"> 
  <link href="../style/foundation-5.5.2/css/foundation.css" rel="stylesheet"> 
  <script src="../style/foundation-5.5.2/js/vendor/modernizr.js"></script>
</head>
<body>
       
<!-- topbar -->
<? include("../includes/header.php"); ?>
<!-- end of topbar -->

<div class="row">
  <div class="small-4 large-4 columns">
    <div class="product-image-large">
      <img src="<?= $product['imageURL'] ?>">
    </div>
  </div>
  <div class="small-8 large-8 columns">
    <h4><?= $product['name'] ?> <small><?= date("F d, Y", strtotime($product['createdAt'])) ?></small></h4>
    <div class="row">
      <div class="large-6 columns">
        <div class="text-center">
          <p><?= $product['description'] ?></p>
          <h5>$<?= $product['price'] ?></h5> 
          <form action="../carts/cart.php" method="post">
            <input type="hidden" name="pid" value="<?= $product['id'] ?>">
            <input type="hidden" name="action" value="add">
            <input type='submit' name='postToCartSubmit' value="<?= $buttonMsg ?>" class='button radius' <?= $buttonStatus ?>>
          </form>
        </div>
      </div>
      <div class="large-6 columns">
        <!-- shopping cart -->
        <? include_once("../carts/show.php"); ?>
        <!-- end of shopping cart -->
      </div>
    </div>
  </div>
</div>

<script src="../style/foundation-5.5.2/js/vendor/jquery.js"></script>
<script src="../style/foundation-5.5.2/js/vendor/fastclick.js"></script>
<script src="../style/foundation-5.5.2/js/foundation.min.js"></script>
<script src="../style/foundation-5.5.2/js/foundation.min.js"></script>
<script> $(document).foundation(); </script>
</body>
</html>
