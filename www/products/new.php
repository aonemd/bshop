<?
session_start();
if(!isset($_SESSION["userIsAdmin"])) {
  header("location: ../users/sign_in.php");
  exit();
}

if(isset($_POST['newProductSubmit'])) {
  include("../db/database.php");
  include("../helpers/image_uploader.php");

  $name = mysql_real_escape_string($_POST['name']);
  $price = mysql_real_escape_string($_POST['price']);
  $quantity = mysql_real_escape_string($_POST['quantity']);
  $description = mysql_real_escape_string($_POST['description']);

    $query = "INSERT INTO products (name, price, quantity, description, created_at)
              VALUES ('$name', '$price', '$quantity', '$description', now())";

    if (mysql_query($query, $connection)) {
      // upload avatar image
      $id = mysql_insert_id();

      if ($imagePath = upload_image("imageURL", "../public/products/")) {
        $query = "UPDATE products SET image_url='$imagePath' WHERE id='$id'";
        if(!mysql_query($query, $connection)){
          echo "error".mysql_error()."\n";
        }
      }

      $_SESSION['notice'] = "Created Product Successfully";
      header("location: index.php");
      exit();
    } else {
        $_SESSION['notice'] = "Product error: ".mysql_error()."\n";
        exit();
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Add new Product</title>
  <link href="../style/style.css" rel="stylesheet"> 
  <link href="../style/foundation-5.5.2/css/foundation.css" rel="stylesheet"> 
  <script src="../style/foundation-5.5.2/js/vendor/modernizr.js"></script>
</head>
<body>

<!-- topbar -->
<? include("../includes/header.php"); ?>
<!-- end of topbar -->

<? if(isset($notice)) { ?>
  <div data-alert class="alert-box">
    <? echo $notice; ?>
    <a href="#" class="close">&times;</a>
  </div>
<? } ?>

<div class="large-3 large-centered columns">
  <div class="form-box">
    <div class="row">
      <div class="large-12 columns">
        <form action="" method="post" enctype="multipart/form-data">
          <div class="row">
            <div class="large-12 columns">
              <div class="form-element">
                <h3 class="text-center">Add Product</h3>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="large-12 columns">
              <div class="form-element">
                <input type="text" name="name" placeholder="Product Name">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="large-12 columns">
              <div class="form-element">
               <input type="text" name="price" placeholder="Price">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="large-12 columns">
              <div class="form-element">
                <input type="text" name="quantity" placeholder="Quantity">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="large-12 columns">
              <div class="form-element">
                <input type="text" name="description" placeholder="Brief Description">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="large-12 columns">
              <div class="form-element">
              <input type="file" name="imageURL" placeholder="Product Image">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="large-12 large-centered columns">
              <div class="form-element">
                <input type="submit" name="newProductSubmit" value="Add Product" class="button radius expand">
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<script src="../style/foundation-5.5.2/js/vendor/jquery.js"></script>
<script src="../style/foundation-5.5.2/js/vendor/fastclick.js"></script>
<script src="../style/foundation-5.5.2/js/foundation.min.js"></script>
<script src="../style/foundation-5.5.2/js/foundation.min.js"></script>
<script> $(document).foundation(); </script>
</body>
</html>
