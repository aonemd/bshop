<?
session_start();
include("../helpers/products_helper.php");
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>BShop</title>
  <link href="../style/style.css" rel="stylesheet"> 
  <link href="../style/foundation-5.5.2/css/foundation.css" rel="stylesheet"> 
  <script src="../style/foundation-5.5.2/js/vendor/modernizr.js"></script>
</head>
<body>

<!-- topbar -->
<? include("../includes/header.php"); ?>
<!-- end of topbar -->

<div class="row">
  <div class="large-12 columns">
    <div class="row">

      <!-- products -->     
      <div class="large-8 columns">
        <div class="row">

          <?php include("../db/database.php");

          $query = "SELECT * FROM products";

          $result = mysql_query($query, $connection);
          if (mysql_num_rows($result) > 0) {
              // output data of each row
            while($row = mysql_fetch_array($result)) { 
              $buttonStatus = toggleButton($row, 'Buy')['status'];
              $buttonMsg = toggleButton($row, 'Buy')['msg'];
            ?>
              <div class="small-4 medium-3 large-4 columns">
                <div class="product-image">
                  <img src="<?= $row['image_url'] ?>">
                </div>

                <div class="panel">
                <h5 class="text-wrap"><?= $row['name'] ?></h5>
                <h6 class="subheader">$<?= $row['price'] ?></h6>
                <form action="show.php" method="get">
                  <input type="hidden" name="id" value="<?= $row['id'] ?>">
                  <input type='submit' name='showProduct' value="<?= $buttonMsg ?>" class='button radius expand' <?= $buttonStatus ?>>
                </form>
                </div>
              </div>
            <?php }
          } ?>
  
        </div>
      </div>
      <!-- end of products -->

      <!-- shopping cart -->
      <div class="large-4 small-12 columns">
      <? include_once("../carts/show.php"); ?>
      </div>
      <!-- end of shopping cart -->

    </div>
  </div>
</div>
   
<script src="../style/foundation-5.5.2/js/vendor/jquery.js"></script>
<script src="../style/foundation-5.5.2/js/vendor/fastclick.js"></script>
<script src="../style/foundation-5.5.2/js/foundation.min.js"></script>
<script src="../style/foundation-5.5.2/js/foundation.min.js"></script>
<script> $(document).foundation(); </script>
</body>
</html>
