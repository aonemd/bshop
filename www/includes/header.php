<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title></title>
</head>
<body>
<br>

<div class="row">
  <div class="small-3 medium-3 large-3 columns">
    <h1><a href="/products/index.php">BShop</a></h1>
  </div>
  <div class="small-9 medium-9 large-9 columns">
    <ul class="button-group right">
      <? if(!$_SESSION['userID']) {?>
        <li><a href="/users/sign_in.php" class="button">Sign In</a></li>
        <li><a href="/users/sign_up.php" class="button">Sign Up</a></li>
      <? }else {?>
        <li><a href="/carts/history.php" class="button">Shopping History</a></li>
        <li><a href="/users/profile.php" class="button">Profile</a></li>
        <li><a href="/users/edit.php" class="button">Edit</a></li>
        <li><a href="/users/logout.php" class="button">Logout</a></li>
      <? } ?>
    </ul>
  </div>
</div>

<div class="row">
  <div class="large-12 columns">
    <br><hr>
    <? session_start(); 
      if(isset($_SESSION['notice'])) { ?>
      <div data-alert class="alert-box">
        <?= $_SESSION['notice'] ?>
        <a href="#" class="close">&times;</a>
      </div>
      <? unset($_SESSION['notice']);
        } ?>
  </div>
</div>
  
</body>
</html>
