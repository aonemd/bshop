<?
session_start();
if(!isset($_SESSION["userID"])) {
  $_SESSION['notice'] = "Please Sign In to proceed";
  header("location: ../users/sign_in.php");
  exit();
}

include("../helpers/carts_helper.php");
include("../helpers/products_helper.php");

if(isset($_POST['postToCartSubmit'])) {
  $action = $_POST['action'];

  switch($action) {
    case 'add':
      $pid = $_POST['pid'];
      $product = getProductByID($pid);

      add_item($product);
      header("location: ../products/show.php?id=$pid");
      break;
    case 'checkout':
      checkout();
      $_SESSION['notice'] = "Cart Checked out successfully";
      header("location: ../carts/history.php");
      break;
  }

} 

if (isset($_GET['action'])) {
  $action = $_GET['action'];

  switch($action) {
    case 'remove':
      $pid = $_GET['pid'];
      $product = getProductByID($pid);
      remove_item($product);
      header("location: ../products/show.php?id=$pid");
      break;
    case 'empty': 
      empty_cart();
      $_SESSION['notice'] = "All Items were removed";
      header("location: ../products/index.php");
      break;
  }
}
?>
