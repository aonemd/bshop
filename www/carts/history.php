<?
session_start();
if(!isset($_SESSION["userID"])) {
  header("location: ../users/sign_in.php");
  exit();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Shopping History | BShop</title>
  <link href="../style/style.css" rel="stylesheet"> 
  <link href="../style/foundation-5.5.2/css/foundation.css" rel="stylesheet"> 
  <script src="../style/foundation-5.5.2/js/vendor/modernizr.js"></script>
</head>
<body>

<!-- topbar -->
<? include("../includes/header.php"); ?>
<!-- end of topbar -->

<div class="row">
  <div class="small-6 large-6 small-centered large-centered columns">


  <?php include("../db/database.php");

  $uid = $_SESSION['userID'];
  $query = "SELECT * FROM orders WHERE user_id='$uid' ORDER BY created_at DESC";

  $result = mysql_query($query, $connection);
  if (mysql_num_rows($result) > 0) {
      // output data of each row
    while($row = mysql_fetch_array($result)) { 
      $cart = unserialize($row['cart']);
    ?>
    <div class="row">
      <div class="small-4 medium-4 large-4 columns"><?= date("F d, Y g:m A", strtotime($row['created_at'])) ?></div>
      <div class="small-8 medium-8 large-8 columns">
        <h4>Total: $<?= $row['total_amount'] ?></h4>
        <ul>
          <? foreach($cart as $item) {?>
          <li><?= $item['name'] ?>: <?= $item['quantity'] ?> item(s)</li>
          <? } ?>
        </ul>
      </div>
    </div>

    <hr/>

    <?php }
  } ?>

  </div>
</div>
     
<script src="../style/foundation-5.5.2/js/vendor/jquery.js"></script>
<script src="../style/foundation-5.5.2/js/vendor/fastclick.js"></script>
<script src="../style/foundation-5.5.2/js/foundation.min.js"></script>
<script src="../style/foundation-5.5.2/js/foundation.min.js"></script>
<script> $(document).foundation(); </script>
</body>
</html>
