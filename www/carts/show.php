<?

include("../helpers/carts_helper.php");

session_start();

$cart = $_SESSION['cart'];
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title></title>
</head>
<body>
  <!-- shopping cart -->
  <div class="hide-for-small panel">
    <h5 class="subheader">You have <?= count_items() ?> item(s) in your cart </h5>
    <h5 class="subheader">Total Amount: $<?= total_amount() ?></h5>
  </div>

    <? 
    foreach($cart as $item) { 
      while($item['quantity'] > 0) {

    ?>
    <div class="panel callout radius">
      <h6>
        <?= $item['name'] ?>: $<?= $item['price'] ?> 
        <small>
          <a href="/carts/cart.php?action=remove&pid=<?= $item['id'] ?>">remove</a>
        </small>
      </h6>
    </div>
    <? 
        $item['quantity'] -= 1;
      }
    }
    ?>

  <form action="/carts/cart.php" method="post">
    <input type="hidden" name="action" value="checkout">
    <input type="submit" name="postToCartSubmit" value="Checkout" class="button radius expand">
  </form>
  <form action="../carts/cart.php" method="get">
    <input type="hidden" name="action" value="empty">
    <input type="submit" name="getCartSubmit" value="Empty Cart" class="button radius expand">
  </form>
  <!-- end of shopping cart -->
  
</body>
</html>
