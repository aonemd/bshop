BShop
---

Simple store app in PHP


### Installation

#### Configuration
- Create a Database with username and password
- change the variables in www/db/db.secrets.php to match your database config
- Run the server on your host

#### Docker
- If you're using Docker, run `docker-compose up` and it will run everything for you
- The server will run on `localhost:8080/`

#### Creating the tables and the data
- Go to `/db/migrations.php` to run the migrations
- To generate some data, run the seed file on `/db/seed.php`
